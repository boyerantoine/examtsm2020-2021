#----------------------------------------------
#PART1

import matplotlib.pyplot as plt
import os
import numpy as np
from numpy.random import randint

class bonds(object):                                                                                                    #create bond class

    def __init__(self, amount_minimum, maturity_minimum, interest_rate):                                                #assign main characteristics
        self.amount_minimum = amount_minimum
        self.maturity_minimum = maturity_minimum
        self.interest_rate = interest_rate

short_term_bonds = bonds(250, 2, 0.015)                                                                                   #create short term bond
long_term_bonds = bonds(1000, 5, 0.03)                                                                                    #create long term bond

def investment_value_sb(amount_investment, year, interest_rate):                                                        #assign function that compute short term bond value at time T
        if amount_investment >= short_term_bonds.amount_minimum:
            return amount_investment*(1 + interest_rate)**year


def investment_value_lb(amount_investment, year, interest_rate):                                                        #assign function that compute long term bond value at time T
    if amount_investment >= long_term_bonds.amount_minimum:
        return amount_investment * (1 + interest_rate) ** year

years_1 = [t for t in range(50)]                                                                                        #create short term bond list of 50 years, starting with 0

sb_list = [investment_value_sb(short_term_bonds.amount_minimum,t ,short_term_bonds.interest_rate) for t in range(50)]   #create list of 50 value for the short term bond, starting with 250

years_2 = [t for t in range(50)]                                                                                        #create long term bond list of 50 years, starting with 0

lb_list = [investment_value_lb(long_term_bonds.amount_minimum,t ,long_term_bonds.interest_rate) for t in range(50)]     #create list of 50 value for the long term bond, starting with 1000



plt.plot(years_1,sb_list,label='ST')       #ST stands for short-term                                                                               #plot the short term bond's evolution over 50 years

plt.plot(years_1,sb_list,label='ST')                                                                                    #plot the short term bond's evolution over 50 years

plt.title("Return on investment of a short-term bond")
plt.xlabel("Maturity in Years")
plt.ylabel("Value in Dollars")
plt.show()


plt.plot(years_2,lb_list, label='LT')      #LT stands for long-term                                                                      #plot the long term bond's evolution over 50 years

plt.plot(years_2,lb_list, label='LT')                                                                                   #plot the long term bond's evolution over 50 years

plt.title("Return on investment of a long-term bond")
plt.xlabel("Maturity in Years")
plt.ylabel("Value in Dollars")
plt.show()
#----------------------------------------------


