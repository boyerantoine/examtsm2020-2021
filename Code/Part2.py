#----------------------------------------------
#PART2
import os
import numpy as np
import pandas as pd
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib import style
import os


StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockFdx, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])

"""Gives the dates but only excluding weekend so goes only up to 2020-11-05 which is off by almost 2 months"""
#MyDataframeGoogle['date'] = pd.date_range(start='9/1/2016', periods=len(MyDataframeGoogle), freq='C')

"""Gives the dates excluding week ends and US bank holidays, going straight to 2021-01-08, only 7 days off"""
us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())
GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq= us_bd)
FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq= us_bd)
IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq= us_bd)
KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq= us_bd)
MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq= us_bd)
NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq= us_bd)
XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq= us_bd)

"""Gives the dates but only excluding weekend so goes only up to 2020-11-05 which is off by almost 2 months"""
plt.plot(GOOGL['date'], GOOGL['high'], label = "Google")
plt.plot(FDX['date'], FDX['high'], label = "FDX")
plt.plot(IBM['date'], IBM['high'], label = "IBM")
plt.plot(KO['date'], KO['high'], label = "KO")
plt.plot(MS['date'], MS['high'], label = "MS")
plt.plot(NOK['date'], NOK['high'], label = "NOK")
plt.plot(XOM['date'], XOM['high'], label = "XOM")
plt.xlabel('Date')
plt.ylabel('High price')
plt.legend()
plt.title('Plot of all the stock prices over the period')
plt.show()

# PART 2 USING CLASSES
                                                                 #Prints the entire row for a given date or the next available date in the dataframe

class Stocks:
    def __init__(self, Term, Ticker, Amount, Date):                                                                     #Common arguments to stocks = Name, term, Amount, number of stocks, and date
        self.Term = Term
        self.Ticker = Ticker
        self.Amount = Amount
        self.Date = datetime.strptime(Date, "%Y-%m-%d")
        self.StartDate = self.Date
        self.EndDate = self.Date - timedelta(days=-(self.Term))
        self.Number = round((self.Amount / (self.Ticker.loc[XOM["date"] == self.Date, "high"])),4)

    def date(self):
        while self.Date not in set(self.Ticker['date']):
            self.Date = self.Date - timedelta(days=1)
        else:
            self.Date = self.Date
            return self.Date

    def price(self):
        return self.Ticker.loc[XOM["date"] == self.Date, "high"]

    def rreturn(self):
        PriceBuy = float(self.Ticker.loc[XOM["date"] == self.StartDate, "high"])
        print(PriceBuy)
        PriceSell = float(self.Ticker.loc[XOM["date"] == self.EndDate, "high"])
        print(PriceSell)
        return round((PriceSell - PriceBuy), 4)

test=Stocks(75,XOM,1000,"2016-09-01")                                                                          #Prints the high price for a given date
print(test.rreturn())