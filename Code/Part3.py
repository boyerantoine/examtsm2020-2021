#----------------------------------------------
#PART3

import random
from random import choice, uniform, randint
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os
import numpy as np
import pandas as pd
from numpy.random import randint


class Bonds(object):                                                                                                    #create bond class

    def __init__(self, amount, maturity, interest_rate):                                                #assign main characteristics
        self.amount = amount
        self.maturity = maturity
        self.interest_rate = interest_rate


    def investment_value_sb(self):                                                        #assign function that compute short term bond value at time T
        if self.amount >= 250:
            return self.amount*(1 + self.interest_rate)**self.maturity
        else: print("You have not enough budget")

    def investment_value_lb(self):                                                        #assign function that compute long term bond value at time T
        if self.amount>=1000:
            return self.amount * (1 + self.interest_rate) ** self.maturity
        else: print("You have not enough budget")

short_term_bonds = Bonds(250, 2, 0.015)  # create short term bond
long_term_bonds = Bonds(1000, 5, 0.03)   # create long term bond

StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockFdx, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])

"""Gives the dates but only excluding weekend so goes only up to 2020-11-05 which is off by almost 2 months"""

"""Gives the dates excluding week ends and US bank holidays, going straight to 2021-01-08, only 7 days off"""
us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())
GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq= us_bd)
FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq= us_bd)
IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq= us_bd)
KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq= us_bd)
MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq= us_bd)
NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq= us_bd)
XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq= us_bd)

"""Gives the dates but only excluding weekend so goes only up to 2020-11-05 which is off by almost 2 months"""
plt.plot(GOOGL['date'], GOOGL['high'], label = "Google")
plt.plot(FDX['date'], FDX['high'], label = "FDX")
plt.plot(IBM['date'], IBM['high'], label = "IBM")
plt.plot(KO['date'], KO['high'], label = "KO")
plt.plot(MS['date'], MS['high'], label = "MS")
plt.plot(NOK['date'], NOK['high'], label = "NOK")
plt.plot(XOM['date'], XOM['high'], label = "XOM")
plt.xlabel('Date')
plt.ylabel('High price')
plt.legend()
plt.title('Plot of all the stock prices over the period')
plt.show()

# PART 2 USING CLASSES
                                                                 #Prints the entire row for a given date or the next available date in the dataframe

class Stocks:
    def __init__(self, Term, Ticker, Amount, Date):                                                                     #Common arguments to stocks = Name, term, Amount, number of stocks, and date
        self.Term = Term
        self.Ticker = Ticker
        self.Amount = Amount
        self.Date = datetime.strptime(Date, "%Y-%m-%d")
        self.StartDate = self.Date
        self.EndDate = self.Date - timedelta(days=-(self.Term))
        self.Number = round((self.Amount / (self.Ticker.loc[XOM["date"] == self.Date, "high"])),4)

    def date(self):
        while self.Date not in set(self.Ticker['date']):
            self.Date = self.Date - timedelta(days=1)
        else:
            self.Date = self.Date
            return self.Date

    def price(self):
        return self.Ticker.loc[XOM["date"] == self.Date, "high"]                                                        #loc is a function in pandas that allows selecting data by label or by a conditional statement

    def rreturn(self):
        PriceBuy = float(self.Ticker.loc[XOM["date"] == self.StartDate, "high"])
        print(PriceBuy)
        PriceSell = float(self.Ticker.loc[XOM["date"] == self.EndDate, "high"])
        print(PriceSell)
        return round(((PriceSell - PriceBuy)/PriceBuy), 4)




# An aggressive investor will only invest in stocks.
# First a randomly chosen stock will be selected.
# Then, depending on the investor remaining budget,
# a random amount of stocks between 0 and the maximum of stocks that investor would be able to buy
# is bought. This is repeated until he has less than 100$ available.

class Aggressive(object):
    def __init__(self, Budget, Date, Term):
        self.Budget = Budget
        self.Date = Date
        self.Term = Term
        self.StartDate = self.Date

    def portfolio(self):
        randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}
        roi = 0
        while self.Budget >=100:
            tick = choice(list(randomstock))
            randomAmount = uniform(0, self.Budget)
            stockInvestment = Stocks(self.Term, randomstock[tick], randomAmount, self.Date)
            self.Budget = self.Budget - randomAmount
            roi = roi + round(Stocks.rreturn(),2)
        return float(round(roi, 4))

class Defensive(object):
    def __init__(self, Budget, Date, Term):
        self.Budget = Budget
        self.Date = Date
        self.Term = Term
        self.StartDate = self.Date

    def portfolio(self):
        roi = 0
        while self.Budget >=1000 :
            randomBond = random.choice(["short_term_bonds", "long_term_bonds"])
            print(randomBond)
            if str(randomBond)=="short_term_bonds" :
                randomAmount = randint(250, self.Budget)
                print(randomAmount)
                ShortTermBondInvestment = Bonds(randomAmount, 2, 0.015)
                print((ShortTermBondInvestment.investment_value_sb()/randomAmount)-1)
                roi = roi +(ShortTermBondInvestment.investment_value_sb()/randomAmount)-1
                self.Budget = self.Budget - randomAmount
            if str(randomBond)=="long_term_bonds" :
                randomAmount = randint(1000, self.Budget)
                print(randomAmount)
                LongTermBondInvestment = Bonds(randomAmount, 5, 0.03)
                print((LongTermBondInvestment.investment_value_lb()/randomAmount)-1)
                roi = roi + (LongTermBondInvestment.investment_value_lb()/randomAmount)-1
                self.Budget = self.Budget - randomAmount
        while self.Budget >=250 :
            randomAmount = randint(250, self.Budget)
            print(randomAmount)
            ShortTermBondInvestment = Bonds(randomAmount, 2, 0.015)
            print((ShortTermBondInvestment.investment_value_sb()/randomAmount)-1)
            roi = roi + (ShortTermBondInvestment.investment_value_sb()/randomAmount)-1
            self.Budget = self.Budget - randomAmount
            return roi+100

# SIMULATION:
# Model 500 aggressive investors,
print("500 aggressive investors: ")
AggressiveInvestment = []
for i in range(0, 500):                                                 # Create range from the 5 years to 50 years
    AggressiveSimulation = Aggressive(5000, "2016-09-01", 1505)
    AggressiveInvestment.append(AggressiveSimulation.portfolio())
print(AggressiveInvestment)
print("Average Return: ")
print(round(sum(AggressiveInvestment) / len(AggressiveInvestment), 2))
# 500 mixed investors and
print("500 Mixed investors: ")
MixedInvestment = []
for i in range(0, 500):                                                 # Create range from the 5 years to 50 years
    MixedSimulation = Mixed(5000, "2016-09-01", 1505)
    MixedInvestment.append(MixedSimulation.portfolio())                 # Append is used such that an item can be added at the end of the list
print(MixedInvestment)
print("Average Return: ")
print(round(sum(MixedInvestment) / len(MixedInvestment), 2))
# 500 defensive investors
print("500 Defensive investors: ")
DefensiveInvestment = []
for i in range(0, 500):                                                 # Create range from the 5 years to 50 years
    DefensiveSimulation = Defensive(5000, "2016-09-01", 1505)
    DefensiveInvestment.append(DefensiveSimulation.portfolio())
print(DefensiveInvestment)
print("Average Return: ")
print(round(sum(DefensiveInvestment) / len(DefensiveInvestment), 2))
